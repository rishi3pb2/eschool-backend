const mongoose = require("mongoose");
const chalk = require("chalk");
const dotenv = require("dotenv");

dotenv.config();

const app = require("./app");

console.log(chalk.blue(app.get("env")));

process.on("uncaughtException", (err) => {
  console.log("UNCAUGHT EXCEPTION! Reload the server");
  console.log(err.name, err.message);
  process.exit(1);
});

const DB = process.env.DATABASE.replace(
  "<PASSWORD>",
  process.env.DATABASE_PASSWORD
);

mongoose
  .connect(DB, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log(chalk.bold.cyanBright("Database Connected"));
  });
const port = process.env.PORT || 80;

app.listen(port, "127.0.0.1", (err) => {
  console.log(chalk.bold.yellowBright(`Listening to port ${port}`));
});

process.on("unhandledRejection", (err) => {
  console.log("UNHANDELED REJECTION! Try to reload the server");
  console.log(err.name, err.message);
  server.close(() => {
    process.exit(1);
  });
});
