module.exports = {
  AuthRoutes: require("./auth.routes"),
  UserRoutes: require("./user.routes"),
  TeacherRoutes: require("./teacher.routes"),
  StudentRoutes: require("./student.routes"),
};
