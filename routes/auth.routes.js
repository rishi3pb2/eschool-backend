const express = require("express");
const passport = require("passport");
const authController = require("../controller/auth/auth.controller");

const router = express.Router();

router.post("/signup", authController.signup);
router.post("/login", authController.login);

router.post("/forgotPassword", authController.forgotPassword);
router.patch("/resetPassword/:token", authController.resetPassword);

router.get(
  "/google",
  passport.authenticate(
    "google",
    {
      scope: [
        "https://www.googleapis.com/auth/userinfo.profile",
        "https://www.googleapis.com/auth/userinfo.email",
      ],
    },
    { session: false }
  )
);
router.get(
  "/google/redirect",
  passport.authenticate("google"),
  authController.GoogleSignin
);

router.patch(
  "/updateMyPassword",
  authController.protect,
  authController.updatePassword
);

module.exports = router;
