const express = require("express");
const upload = require("../utils/multer");
const {
  subjectController,
  assignmentController,
  submissionController,
  onlineclassController,
} = require("../controller/user/teacher");
const authController = require("../controller/auth/auth.controller");

const router = express.Router();

router
  .route("/assignment/:id?")
  .get(
    [authController.protect, authController.restrictTo("teacher")],
    assignmentController.getAssignments
  )
  .post(
    [authController.protect, authController.restrictTo("teacher")],
    upload.single("testDoc"),
    assignmentController.createAssignment
  )
  .patch(
    [authController.protect, authController.restrictTo("teacher")],
    assignmentController.updateAssignment
  )
  .delete(
    [authController.protect, authController.restrictTo("teacher")],
    assignmentController.deleteAssignment
  );

router.patch(
  "/subject/:id/addStudents",
  [authController.protect, authController.restrictTo("teacher")],
  subjectController.assignStudentsToSubject
);

router.patch(
  "/subject/:id/removeStudent",
  [authController.protect, authController.restrictTo("teacher")],
  subjectController.removeStudentFormSubject
);

router
  .route("/subject/:id?")
  .get(
    [authController.protect, authController.restrictTo("teacher")],
    subjectController.getSubjects
  )
  .post(
    [authController.protect, authController.restrictTo("teacher")],
    subjectController.createNewSubject
  )
  .patch(
    [authController.protect, authController.restrictTo("teacher")],
    subjectController.updateSubject
  )
  .delete(
    [authController.protect, authController.restrictTo("teacher")],
    subjectController.deleteSubject
  );

router
  .route("/submission")
  .post(
    [authController.protect, authController.restrictTo("teacher")],
    submissionController.FetchSubmission
  );
router
  .route("/submission/:id")
  .post(
    [authController.protect, authController.restrictTo("teacher")],
    submissionController.FetchSubmissionById
  )
  .patch(
    [authController.protect, authController.restrictTo("teacher")],
    submissionController.AssignMarks
  );

router
  .route("/class")
  .post(
    [authController.protect, authController.restrictTo("teacher")],
    onlineclassController.getOnlineClasses
  );
router
  .route("/class/create")
  .post(
    [authController.protect, authController.restrictTo("teacher")],
    onlineclassController.createOnlineClass
  );

router
  .route("/class/:id")
  .post(
    [authController.protect, authController.restrictTo("teacher")],
    onlineclassController.getOnlineClasses
  )
  .patch(
    [authController.protect, authController.restrictTo("teacher")],
    onlineclassController.updateOnlineClass
  )
  .delete(
    [authController.protect, authController.restrictTo("teacher")],
    onlineclassController.deleteOnlineClass
  );

module.exports = router;
