const express = require("express");
const upload = require("../utils/multer");
const {
  subjectController,
  assignmentController,
  onlineclassController,
} = require("../controller/user/student");
const authController = require("../controller/auth/auth.controller");

const router = express.Router();

router
  .route("/subject/:id?")
  .get(
    [authController.protect, authController.restrictTo("student")],
    subjectController.getSubjects
  );

router
  .route("/assignment/:id?")
  .post(
    [authController.protect, authController.restrictTo("student")],
    assignmentController.getAssignments
  );

router
  .route("/submission/:id?")
  .post(
    [authController.protect, authController.restrictTo("student")],
    upload.single("document"),
    assignmentController.mySubmission
  )
  .patch(
    [authController.protect, authController.restrictTo("student")],
    upload.single("document"),
    assignmentController.updateMySubmission
  );

router.route("/class").get(onlineclassController.getOnlineClasses);
module.exports = router;
