const express = require("express");
const path = require("path");
const morgan = require("morgan");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const AppError = require("./utils/appError");
const globalErrorHandler = require("./controller/error/error.controller");
const passport = require("passport");
const app = express();
const { AuthRoutes, UserRoutes, TeacherRoutes, StudentRoutes } = require("./routes");
require("./utils/passport");

if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}

app.use(cors());
app.use(express.json());
app.use(cookieParser());
app.use(passport.initialize());
app.use(express.urlencoded({ extended: true, limit: "10kb" }));
app.use(express.static(path.join(__dirname, "uploads")));

app.use("/api/auth", AuthRoutes);
app.use("/api/user", UserRoutes);
app.use("/api/teacher", TeacherRoutes);
app.use("/api/student", StudentRoutes);

app.use("/", (req, res) => {
  res.send("API is running");
});

app.all("*", (req, res, next) => {
  next(new AppError(`Can't find ${req.originalUrl} on this server!`, 404));
});

app.use(globalErrorHandler);

module.exports = app;
