const db = require("../../../models");
const catchAsync = require("../../../utils/catchAsync");
const AppError = require("../../../utils/appError");

const factory = require("../../../utils/factory");

const Subject = db.subject;
const User = db.user;

exports.getSubjects = catchAsync(async (req, res, next) => {
  let data;
  if (req.params.id) {
    data = await Subject.findOne({
      _id: req.params.id,
      teacherId: req.user.id,
    });
  } else {
    data = await Subject.find({ teacherId: req.user.id });
  }
  res.status(201).json({
    status: "success",
    data: {
      data,
    },
  });
});

exports.createNewSubject = catchAsync(async (req, res, next) => {
  const subject = await Subject.create({
    name: req.body.name,
    description: req.body.description,
    teacherId: req.user.id,
  });
  res.status(200).json({
    status: "success",
    data: {
      data: subject,
    },
  });
});

exports.assignStudentsToSubject = catchAsync(async (req, res, next) => {
  const { id } = req.params;
  const students = req.body.students;
  const subject = await Subject.findOne({ _id: id, teacherId: req.user.id });
  if (!subject) return next(new AppError("Subject not found", 400));
  subject.studentsId = [...subject.studentsId, ...students];
  for (let i = 0; i < students.length; i++) {
    const student = await User.findOne({ _id: students[i] });
    student.subjects = [...student.subjects, subject.id];
    await student.save();
  }
  await subject.save();
  return res.status(200).json({
    status: "success",
    data: {
      data: subject,
    },
  });
});

exports.removeStudentFormSubject = catchAsync(async (req, res, next) => {
  const { id } = req.params;
  const student = req.body.studentId;
  const subject = await Subject.findOne({ _id: id, teacherId: req.user.id });
  if (!subject) return next(new AppError("Subject not found", 400));
  const stIdx = subject.studentsId.indexOf(student);
  if (stIdx !== -1) {
    subject.studentsId.splice(stIdx, stIdx + 1);
  } else {
    return next(new AppError("Student not found", 400));
  }
  await subject.save();
  return res.status(200).json({
    status: "success",
    data: {
      data: subject,
    },
  });
});

exports.updateSubject = factory.updateOne(Subject, { teacherId: "" });
exports.deleteSubject = factory.deleteOne(Subject, { teacherId: "" });
