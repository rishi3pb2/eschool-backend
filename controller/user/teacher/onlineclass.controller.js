const db = require("../../../models");
const catchAsync = require("../../../utils/catchAsync");
const AppError = require("../../../utils/appError");
const factory = require("../../../utils/factory");

const OnlineClass = db.onlineClass;
const Subject = db.subject;

exports.getOnlineClasses = factory.getAll(OnlineClass, { subjectId: "" });
exports.getOnlineClassById = factory.getOne(OnlineClass, { subjectId: "" });

exports.createOnlineClass = catchAsync(async (req, res, next) => {
  const subject = await Subject.findById(req.body.subjectId);
  if (subject.teacherId != req.user.id) {
    return next(
      new AppError("You can only create classes for your subject", 400)
    );
  }
  const onlineClass = await OnlineClass.create({
    subjectId: req.body.subjectId,
    title: req.body.title,
    fromTime: req.body.fromTime,
    toTime: req.body.toTime,
  });
  res.status(200).json({
    status: "success",
    data: {
      data: onlineClass,
    },
  });
});

exports.updateOnlineClass = factory.updateOne(OnlineClass, {
  teacherId: "",
  subjectId: "",
});
exports.deleteOnlineClass = factory.deleteOne(OnlineClass, {
  teacherId: "",
  subjectId: "",
});
