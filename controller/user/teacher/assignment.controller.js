const db = require("../../../models");
const catchAsync = require("../../../utils/catchAsync");
const AppError = require("../../../utils/appError");
const factory = require("../../../utils/factory");

const Assignment = db.assignment;
const Subject = db.subject;

exports.getAssignments = catchAsync(async (req, res, next) => {
  let data;
  const filterObj = {
    teacherId: req.user.id,
    subjectId: req.body.subjectId,
  };
  if (req.query.type) filterObj.type = req.query.type;

  if (req.params.id) {
    data = await Assignment.findOne(filterObj);
  } else {
    data = await Assignment.find(filterObj);
  }

  res.status(201).json({
    status: "success",
    data: {
      data,
    },
  });
});

exports.createAssignment = catchAsync(async (req, res, next) => {
  const testDoc = req.file.path;
  const testLink = req.body.testLink;
  if (testDoc && testLink)
    return next(new AppError("Only one is required TestLink or TestDoc", 500));
  if (testDoc) req.body.testDoc = testDoc;

  // check if teacher belong to this subject or not
  const subject = await Subject.findOne({
    _id: req.body.subjectId,
  });
  if (!subject) return next(new AppError("Subject not found", 400));
  if (subject.teacherId != req.user.id)
    return next(new AppError("This subject did not belong to you", 400));
  const assignment = await Assignment.create(req.body);
  res.status(200).json({
    status: "success",
    data: {
      data: assignment,
    },
  });
});

exports.updateAssignment = catchAsync(async (req, res, next) => {
  const subject = await Subject.findOne({
    _id: req.body.subjectId,
  });
  if (!subject) return next(new AppError("Subject not found", 400));
  if (subject.teacherId != req.user.id)
    return next(new AppError("This subject did not belong to you", 400));
  factory.updateOne(Assignment);
});

exports.deleteAssignment = catchAsync(async (req, res, next) => {
  const subject = await Subject.findOne({
    _id: req.body.subjectId,
  });
  if (!subject) return next(new AppError("Subject not found", 400));
  if (subject.teacherId != req.user.id)
    return next(new AppError("This subject did not belong to you", 400));
  factory.deleteOne(Assignment);
});
