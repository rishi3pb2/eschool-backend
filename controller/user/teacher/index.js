module.exports = {
  assignmentController: require("./assignment.controller"),
  subjectController: require("./subject.controller"),
  submissionController: require("./submission.controller"),
  onlineclassController: require("./onlineclass.controller"),
};
