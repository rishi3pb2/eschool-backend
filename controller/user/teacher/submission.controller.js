const db = require("../../../models");
const catchAsync = require("../../../utils/catchAsync");
const AppError = require("../../../utils/appError");
const factory = require("../../../utils/factory");

const Submission = db.submission;

exports.FetchSubmission = factory.getAll(Submission, {
  assignmentId: "",
});

exports.FetchSubmissionById = factory.getOne(Submission);

exports.AssignMarks = catchAsync(async (req, res, next) => {
  const submission = await Submission.findByIdAndUpdate(req.params.id, {
    score: req.body.score,
  });
  res.status(200).json({
    status: "success",
    data: {
      data: submission,
    },
  });
});
