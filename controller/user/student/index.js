module.exports = {
  subjectController: require("./subject.controller"),
  assignmentController: require("./assignment.controller"),
  onlineclassController: require("./onlineclass.controller"),
};
