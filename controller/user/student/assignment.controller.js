const db = require("../../../models");
const catchAsync = require("../../../utils/catchAsync");
const AppError = require("../../../utils/appError");
const factory = require("../../../utils/factory");

const Assignment = db.assignment;
const User = db.user;
const Subject = db.subject;
const Submission = db.submission;

exports.getAssignments = catchAsync(async (req, res, next) => {
  const { id } = req.params;
  const { type } = req.query;
  const filter = {};
  if (type) filter.type = type;

  // checking if student belong to this subject
  const subject = await Subject.findOne({
    _id: req.body.subjectId,
    studentsId: req.user.id,
  });
  if (!subject) return next(new AppError("You didn't belong to this subject"));
  filter.subjectId = subject._id;
  let data;
  if (id) {
    filter._id = id;
    data = await Assignment.findOne(filter).populate({
      path: "submissions",
      select: "assignmentId document submissionTime score studentId",
    });

    // only logged in user submissions in response
    data.submissions = data.submissions.filter(
      (el) => el.studentId == req.user.id
    );
  } else {
    data = await Assignment.find(filter);

    // don't want submissions when fetching all
    data = data.map((el) => {
      el.submissions = [];
      return el;
    });
  }

  res.status(200).json({
    status: "success",
    data: { data },
  });
});

exports.mySubmission = catchAsync(async (req, res, next) => {
  const { id } = req.params;
  const filter = {};

  // checking if student belong to this subject

  const subject = await Subject.findOne({
    _id: req.body.subjectId,
    studentsId: req.user.id,
  });

  if (!subject) return next(new AppError("You didn't belong to this subject"));
  filter.subjectId = subject._id;
  filter._id = id;

  const data = await Assignment.findOne(filter);

  const submission = await Submission.create({
    document: req.file.path,
    submissionTime: new Date(Date.now()),
    assignmentId: data.id,
    studentId: req.user.id,
    subjectId: subject._id,
  });

  data.submissions.push(submission._id);
  req.user.mySubmission.push(submission._id);

  data.markModified("submissions");
  req.user.markModified("mySubmission");

  (await data.save()) && (await req.user.save());
  if (data && req.user) {
    res.status(200).json({
      status: "success",
      data: {
        data: submission,
      },
    });
  }
});

exports.updateMySubmission = catchAsync(async (req, res, next) => {
  const { id } = req.params;
  const updateObj = {};
  if (req.file && req.file.path) {
    updateObj.document = req.file.path;
    updateObj.submissionTime = new Date(Date.now());
  }

  // checking if student belong to this subject

  const subject = await Subject.findOne({
    _id: req.body.subjectId,
    studentsId: req.user.id,
  });

  if (!subject) return next(new AppError("You didn't belong to this subject"));

  // check if assignment deadline is meet

  const assignment = await Assignment.findById(req.body.assignmentId);
  if (new Date(assignment.end_time) < new Date(Date.now())) {
    return next(new AppError("No more submissions after deadline", 400));
  }

  const submission = await Submission.findByIdAndUpdate(id, updateObj);
  if (submission) {
    res.status(200).json({
      status: "success",
      data: {
        data: submission,
      },
    });
  }
});
