const db = require("../../../models");
const catchAsync = require("../../../utils/catchAsync");
const AppError = require("../../../utils/appError");
const factory = require("../../../utils/factory");

const OnlineClass = db.onlineClass;

exports.getOnlineClasses = factory.getAll(OnlineClass, { subjectId: "" });
