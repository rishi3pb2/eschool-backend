const db = require("../../../models");
const catchAsync = require("../../../utils/catchAsync");
const AppError = require("../../../utils/appError");
const factory = require("../../../utils/factory");

const Subject = db.subject;

exports.getSubjects = catchAsync(async (req, res, next) => {
  const { id } = req.params;
  let data;
  if (id) {
    data = await Subject.findOne({
      _id: id,
      studentsId: req.user.id,
    });
  } else {
    data = await Subject.find({
      studentsId: req.user.id,
    });
  }
  res.status(200).json({
    status: "success",
    data: { data },
  });
});
