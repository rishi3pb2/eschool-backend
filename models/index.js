const db = {};

db.user = require("./user");
db.onlineClass = require("./onlineClass");
db.assignment = require("./assignment");
db.subject = require("./subject");
db.submission = require("./submission");

module.exports = db;
