const mongoose = require("mongoose");

const submissionSchema = new mongoose.Schema(
  {
    subjectId: {
      type: mongoose.Types.ObjectId,
      required: true,
    },
    assignmentId: {
      type: mongoose.Types.ObjectId,
      required: true,
    },
    studentId: {
      type: mongoose.Types.ObjectId,
      required: true,
    },
    document: String,
    submissionTime: Date,
    score: {
      type: Number,
      default: 0,
    },
  },
  {
    timestamps: true,
  }
);

const Submission = mongoose.model("Submission", submissionSchema);

module.exports = Submission;
