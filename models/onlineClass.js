const mongoose = require("mongoose");

const onlineClassSchema = new mongoose.Schema(
  {
    title: String,
    fromTime: {
      type: Date,
      required: true,
    },
    toTime: {
      type: Date,
      required: true,
    },
    classlink: String,
    subjectId: mongoose.Types.ObjectId,
  },
  {
    timestamps: true,
  }
);

const OnlineClass = mongoose.model("OnlineClass", onlineClassSchema);

module.exports = OnlineClass;
