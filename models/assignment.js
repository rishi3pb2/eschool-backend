const mongoose = require("mongoose");

const assignmentSchema = new mongoose.Schema(
  {
    subjectId: {
      type: mongoose.Types.ObjectId,
      required: true,
    },
    type: {
      type: String,
      enum: ["test", "assignment"],
      default: "test",
      lowercase: true,
    },
    testlink: {
      type: String,
    },
    testDoc: {
      type: String,
    },
    name: {
      type: String,
      required: true,
    },
    start_time: {
      type: Date,
      required: true,
    },
    end_time: {
      type: Date,
      required: true,
    },
    submissions: [
      {
        type: mongoose.Types.ObjectId,
        ref: "Submission",
      },
    ],
  },
  {
    timestamps: true,
  }
);

const Assignment = mongoose.model("Assignment", assignmentSchema);

module.exports = Assignment;
