const catchAsync = require("./catchAsync");
const { Model } = require("mongoose");
const AppError = require("./appError");
const APIFeatures = require("./apiFeatures");

exports.deleteOne = (Model, filter = {}) =>
  catchAsync(async (req, res, next) => {
    filter._id = req.params.id;
    if (Object.keys(filter).includes("teacherId"))
      filter.teacherId = req.user.id;

    if (Object.keys(filter).includes("subjectId"))
      filter.subjectId = req.body.subjectId;
    const doc = await Model.findOneAndDelete(filter);
    if (!doc) {
      return next(new AppError("No document found with that ID", 404));
    }
    res.status(204).json({
      status: "success",
      data: null,
    });
  });

exports.updateOne = (Model, filter = {}) =>
  catchAsync(async (req, res, next) => {
    filter._id = req.params.id;
    if (Object.keys(filter).includes("teacherId"))
      filter.teacherId = req.user.id;
    if (Object.keys(filter).includes("subjectId"))
      filter.subjectId = req.body.subjectId;
    const doc = await Model.findOneAndUpdate(filter, req.body, {
      new: true,
      runValidators: true,
    });

    if (!doc) {
      return next(new AppError("No document found with that ID", 404));
    }

    res.status(200).json({
      status: "success",
      data: {
        doc,
      },
    });
  });

exports.getOne = (Model, filter = {}) =>
  catchAsync(async (req, res, next) => {
    if (Object.keys(filter).includes("subjectId"))
      filter.subjectId = req.body.subjectId;
    filter._id = req.params.id;
    let query = await Model.findOne(filter);
    const doc = await query;
    if (!doc) {
      return next(new AppError("No document found with that ID", 404));
    }

    res.status(200).json({
      status: "success",
      data: {
        data: doc,
      },
    });
  });

exports.getAll = (Model, filter = {}) =>
  catchAsync(async (req, res, next) => {
    if (Object.keys(filter).includes("assignmentId"))
      filter.assignmentId = req.body.assignmentId;

    if (Object.keys(filter).includes("subjectId"))
      filter.subjectId = req.body.subjectId;

    const features = new APIFeatures(await Model.find(filter), req.query);
    res.status(201).json({
      status: "success",
      data: {
        data: features.query,
      },
    });
  });

exports.createOne = (Model) =>
  catchAsync(async (req, res, next) => {
    const doc = await Model.create(req.body);
    res.status(201).json({
      status: "success",
      data: {
        data: doc,
      },
    });
  });
