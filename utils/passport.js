const passport = require("passport");

// google login
const GoogleStrategy = require("passport-google-oauth20").Strategy;

const db = require("../models");
const User = db.user;
const backendUrl = "http://localhost:3333";

passport.serializeUser((user, done) => {
  done(null, user._id);
});

passport.deserializeUser((id, done) => {
  User.findById(id).then((user) => {
    done(null, user);
  });
});

passport.use(
  new GoogleStrategy(
    {
      callbackURL: `${backendUrl}/api/auth/google/redirect`,
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      proxy: true,
    },
    async (accessToken, refreshToken, profile, done) => {
      try {
        let user = await User.findOne({ email: profile.emails[0].value });
        if (user) {
          if (!user.googleId) user.googleId = profile.id;
          done(null, user);
        } else {
          let newUser = new User({
            googleId: profile.id,
            email: profile.emails[0].value,
            name: profile.name.givenName,
          });
          await newUser.save();
          done(null, newUser);
        }
      } catch (error) {
        console.error(error);
      }
    }
  )
);
